<?php include('server.php') 
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Add Product</title>
    <link rel="stylesheet" type="text/css" href="loginstyle.css">
  </head>
  <body>
    <div class="header">
  	  <h2>ADD PRODUCTS</h2>
    </div>
    <form method="post" action="addproduct.php" enctype="multipart/form-data">
  	  <?php include('errors.php'); ?>
      <div class="input-group">
        <label>Name of the Product</label>
        <input type="text" name="name" value="<?php echo $name; ?>">
      </div>
      <div class="input-group">
        <label>Description Of the Product</label>
        <input type="text" name="description_product" value="<?php echo $role; ?>">
      </div>
      <div class="input">
        <label>Image Upload</label>
        <input type="file" name="fileToUpload" id="fileToUpload">
      </div>
  	  <div class="input-group">
  	    <label>Cateory</label>
	      <select name="cateory">
          <option value="volvo">Volvo</option>
          <option value="saab">Saab</option>
          <option value="mercedes">Mercedes</option>
          <option value="audi">Audi</option>
        </select> 
  	  </div>
  	  <div class="input-group">
  	    <button type="submit" class="btn" name="prod_user">Add</button>
  	  </div>
      <p>
        <a href="admin.php">Back</a>
      </p>
    </form>
  </body>
</html>