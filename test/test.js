      $(document).ready(function(){
        $(".nav-tabs a").click(function(){
          $(this).tab('show');
        });
        $('.nav-tabs a').on('shown.bs.tab', function(event){
          var x = $(event.target).text();      
          var y = $(event.relatedTarget).text();  
          $(".act span").text(x);
          $(".prev span").text(y);
        });
      });
      (function() {
        var $window = $(window),
        flexslider = { vars:{} };
        function getGridSize() {
          return (window.innerWidth < 600) ? 2 :
          (window.innerWidth < 900) ? 3 : 4;
        }       
        $(function() {
          SyntaxHighlighter.all();
       });
       $window.load(function() {
          $('.flexslider').flexslider({
          animation: "slide",
          animationLoop: false,
          itemWidth: 210,
          itemMargin: 5,
          minItems: getGridSize(), // use function to pull in initial value
          maxItems: getGridSize() // use function to pull in initial value
        });
    });
 
  // check grid size on resize event
  $window.resize(function() {
    var gridSize = getGridSize();
 
    flexslider.vars.minItems = gridSize;
    flexslider.vars.maxItems = gridSize;
  });
}());