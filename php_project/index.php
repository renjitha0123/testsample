<?php 
  session_start(); 

  if (!isset($_SESSION['usernmae'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: index.php');
  }
  if (isset($_GET['logout'])) {

    session_destroy();
    unset($_SESSION['usernmae']);   
    unset($_SESSION['firstname']);
    unset($_SESSION['lastname']);
    session_unset(); 
    header('location: login.php');
    
  }
?>
<!DOCTYPE html>
<html>
  <head>
	  <title>Home</title>
	  <link rel="stylesheet" type="text/css" href="loginstyle.css">
  </head>
  <body>
    <div class="header">
	    <h2>Home Page</h2>
    </div>
    <div class="content">
  	<!-- notification message -->
  	  <?php if (isset($_SESSION['success'])) : ?>
      <div class="error success" >
      	<h3>
          <?php 
          echo $_SESSION['success']; 
          unset($_SESSION['success']);
          ?>
      	</h3>
      </div>
  	  <?php endif ?>

    <!-- logged in user information -->
      <?php  if (isset($_SESSION['usernmae'])) : ?>
      <p>Welcome User <strong><?php echo $_SESSION['usernmae']; ?></strong></p>
      <p>FirstName<strong><?php echo $_SESSION['firstname']; ?></strong></p>
      <p>LastName<strong><?php echo $_SESSION['lastname']; ?></strong></p>
      <?php
      // unset($_SESSION['usernmae']);
      // unset($_SESSION['firstname']);
      // unset($_SESSION['lastname']);
     ?>
      <p> <a href="index.php?logout=1" style="color: red;">logout</a> </p>
      <?php endif ?>
    </div>		
  </body>
</html>
